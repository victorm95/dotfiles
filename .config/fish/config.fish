if test -e /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.fish
  . "/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.fish"
end

if status is-interactive
  abbr v nvim
  alias ls "exa --icons"
  alias ll "ls -l"

  starship init fish | source
end

