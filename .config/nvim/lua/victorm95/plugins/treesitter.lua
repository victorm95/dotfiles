return {
  'nvim-treesitter/nvim-treesitter',
  build = ':TSUpdate',
  config = function()
    -- require('nvim-treesitter.install').compilers = {'zig'}
    require('nvim-treesitter.configs').setup {
      ensure_installed = {
        'lua',
        'javascript',
        'jsdoc',
        'typescript',
        'tsx',
        'comment',
        'html',
        'css',
        'scss',
        'yaml',
        'toml',
        'dockerfile',
        'markdown',
      },
      highlight = {
        enable = true,
        additional_vim_regex_highlighting  = false,
      },
      indent = {
        enable = true,
      },
    }
  end,
}
