return {
  'nvim-telescope/telescope.nvim', branch = '0.1.x',
  dependencies = {
    'nvim-lua/plenary.nvim',
    { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
  },
  config = function()
    local telescope = require('telescope')
    telescope.setup {
      fzf = {
        override_generic_sorter = true,
        override_file_sorter = true,
      }
    }

    telescope.load_extension('fzf')
  end
}
