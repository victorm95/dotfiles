return {
  'rose-pine/neovim',
  name = 'rose-pine',
  config = function()
    require('rose-pine').setup {
      highlight_groups = {
        ColorColumn = { bg = 'surface' }, -- Default: overlay
        NonText = { fg = 'highlight_med' }, -- Default: muted
      },
    }
  end,
}
