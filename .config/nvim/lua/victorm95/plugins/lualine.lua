-- Rosé pine
local colors = {
  base = '#191724',
  surface = '#1f1d2e',
  overlay = '#26233a',
  muted = '#6e6a86',
  subtle = '#908caa',
  text = '#e0def4',
  love = '#eb6f92',
  gold = '#f6c177',
  rose = '#ebbcba',
  pine = '#31748f',
  foam = '#9ccfd8',
  iris = '#c4a7e7',
  highlight_low = '#21202e',
  highlight_med = '#403d52',
  highlight_high = '#524f67',
}

return {
  'nvim-lualine/lualine.nvim',
  config = function()
    require('lualine').setup{
      options = {
        globalstatus = true,
        component_separators = '',
        section_separators = ' ',
        theme = {
          normal = {
            a = {bg = colors.rose, fg = colors.base},
            b = {bg = colors.overlay, fg = colors.rose},
            c = {bg = colors.muted, fg = colors.text},
          },
          insert = {
            a = {bg = colors.pine, fg = colors.text},
            b = {bg = colors.overlay, fg = colors.pine},
            c = {bg = colors.muted, fg = colors.text},
          },
          visual = {
            a = {bg = colors.foam, fg = colors.base},
            b = {bg = colors.overlay, fg = colors.foam},
            c = {bg = colors.muted, fg = colors.text},
          },
          replace = {
            a = {bg = colors.iris, fg = colors.base},
            b = {bg = colors.overlay, fg = colors.iris},
            c = {bg = colors.muted, fg = colors.text},
          },
          command = {
            a = {bg = colors.gold, fg = colors.base},
            b = {bg = colors.overlay, fg = colors.gold},
            c = {bg = colors.muted, fg = colors.text},
          },
        },
      },
      sections = {
        lualine_a = {'mode'},
        lualine_b = {{'branch', icon = ''}},
        lualine_c = {{'filename', path = 1}},
        lualine_x = {'diagnostics', '"[ %l:%c ]"', '%y'},
        lualine_y = {},
        lualine_z = {},
      },
    }
  end
}
