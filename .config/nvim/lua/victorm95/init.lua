-- Leader key
vim.g.mapleader = ' '

require 'victorm95.lazy'
require 'victorm95.settings'
require 'victorm95.keymappings'
