-- Editor
vim.opt.tabstop = 4
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.shiftround = true
vim.opt.expandtab = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.list = true
vim.opt.listchars = {
  eol = '⤸',
  tab = '››',
  lead = '⋅',
  trail = '⋅',
  extends = '…',
  precedes = '…',
}

-- Theme
vim.opt.termguicolors = true
vim.cmd [[colorscheme rose-pine]]
vim.cmd [[highlight Normal ctermbg=NONE guibg=NONE]]

-- Files
vim.opt.fileencodings = 'utf-8'
vim.opt.fileformats = {'unix', 'dos'}

-- Misc
vim.opt.wrap = false
vim.opt.clipboard = 'unnamedplus'
vim.opt.completeopt = {'menu', 'menuone', 'noselect'}
vim.opt.hidden = true
vim.opt.guicursor = ''
vim.opt.cursorline = true
vim.opt.scrolloff = 8
vim.opt.sidescrolloff = 8
vim.opt.colorcolumn = {80, 120}
vim.opt.undofile = true
vim.opt.swapfile = false
