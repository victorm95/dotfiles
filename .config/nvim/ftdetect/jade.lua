vim.api.nvim_create_autocmd({'BufEnter'}, {
  pattern = {'*.jade'},
  command = 'set filetype=pug',
})
